[https://animalequality.gitlab.io/whatcity/index.html](https://animalequality.gitlab.io/whatcity/index.html)

* Display the cities for a given range of inhabitants.
* Data from [https://unstats.un.org/unsd/demographic-social/products/dyb/dyb_2017/](https://unstats.un.org/unsd/demographic-social/products/dyb/documents/dyb2017/table08.xls) : _Table 8 - Population of capital cities and cities of 100 000 or more inhabitants: latest available year, 1998-2017_.

* Update dataset: `node make-json.js` with a newer version xlsx file.
