const MILLION = Math.pow(10, 6);

function expandSuffix(value) {
    if (("" + value).match(/k/i)) {
        return parseInt(value) * 1000;
    }
    if (("" + value).match(/m/i)) {
        return parseInt(value) * 1000 * 1000;
    }

    return parseInt(value);
}

// https://stackoverflow.com/questions/44134212/
function flattenObject(ob) {
    var toReturn = {};

    for (var i in ob) {
        if (!ob.hasOwnProperty(i)) continue;

        if ((typeof ob[i]) == 'object' && ob[i] !== null) {
            var flatObject = flattenObject(ob[i]);
            for (var x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;

                toReturn[i + '.' + x] = flatObject[x];
            }
        } else {
            toReturn[i] = ob[i];
        }
    }
    return toReturn;
}

// https://stackoverflow.com/questions/38750705/
function filterObject(ob, filter) {
    return Object.keys(ob)
        .filter(key => filter.test(key))
        .reduce((obj, key) => {
            obj[key] = ob[key];
            return obj;
        }, {});
}

function filterByMargin(ob, min, max) {
    Object.keys(ob).forEach(key => {
        var num = ob[key];
        if (num < min || num > max) delete ob[key];
    });

    return ob;
}

function saneMargin(number) {
    number = expandSuffix(number);
    if (number >= 10 * MILLION) return 20;
    if (number >= 8 * MILLION) return 15;
    else if (number >= 4 * MILLION) return 10;
    else return 5;
}

export default (obj, number, margin = null, filter = null) => {
    let copy = flattenObject(obj);
    if (filter) {
        filter = new RegExp(filter, "i");
        copy = filterObject(copy, filter);
    }

    number = expandSuffix(number);
    margin = margin || saneMargin(number);
    var max = (1 + margin/100) * number,
        min =  (1 - margin/100) * number;

    console.log("Looking for results around %d < %d < %d", min, number, max);
    return filterByMargin(copy, min, max);
};
