const readXlsxFile = require('read-excel-file/node');
const fs = require('fs');
 
var data = {};
readXlsxFile('data.xlsx')
    .then((rows) => {
        let i = 5;
        while (i--) rows.shift();

        var continent = null,
            country = null;

        for(let row of rows) {
            var localisation = row[0],
                incity = row[1],
                inarea = row[9];

            if (localisation.match(/^[0-9]/)) {
                continue;
            }

            console.log("Analysing ", row);            
            if (!incity) {
                // This row is a continent or a country
                if (localisation.match(/^[A-Z -]+$/)) {
                    continent = localisation;
                    country = null;
                    data[continent] = {};
                } else {
                    country = localisation.replace(/[0-9]*$/, '');
                    data[continent][country] = {};
                }
                console.log("[skipping] continent = %s, country = %s", continent, country);
                continue;
            }

            console.log("continent = %s, country = %s, incity = %s", continent, country, incity);
            
            if (incity != '...') {
                data[continent][country][localisation + '_proper'] = parseInt(incity);
            }
            if (inarea != '...') {
                data[continent][country][localisation + '_area'] = parseInt(inarea);
            }
        }

        return data;
    })
    .then((data) => {
        fs.writeFileSync("data.json", JSON.stringify(data));
    });

