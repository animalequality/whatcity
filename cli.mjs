import lookup from './lookup.mjs';
import fs from 'fs';
import minimist from 'minimist';

const args = minimist(process.argv.slice(2));
var input = args['i'] || 'data.json',
    margin = args['m'] || null,
    number = args['_'][0],
    filter = args['_'][1] || null;

var obj = JSON.parse(fs.readFileSync(input, 'utf8'));
var matches = lookup(obj, number, margin, filter);
