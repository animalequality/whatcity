import lookup from './lookup.js';

var json = null;
fetch('data.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    json = myJson;
  });

function format(s) {
    return s.replace('_proper', ' (city)')
        .replace('_area', ' (urban area)');
}

function onChange() {
    var number = document.getElementById('number').value,
        filter = document.getElementById('filter').value,
        margin = document.getElementById('margin').value;

    var matches = lookup(json, number, margin, filter);
    var html = Object.entries(matches).map(x => {
        return '<tr><td>' + x[1] + '</td><td>' + format(x[0]) + '</td></tr>';
    });

    console.log(html);
    document.getElementById('results').innerHTML = html.join("\n");
}

function attachEvent() {
    document.getElementById('number').addEventListener('input', onChange, false);
    document.getElementById('filter').addEventListener('input', onChange, false);
    document.getElementById('margin').addEventListener('change', onChange, false);

    // https://stackoverflow.com/questions/14267781
    document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
        const table = document.querySelector('#results');
        Array.from(table.querySelectorAll('tr'))
            .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
            .forEach(tr => table.appendChild(tr) );
    })));
}

const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
    v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
    )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

window.onload = attachEvent;
